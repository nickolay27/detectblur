/*
 * Copyright (c) 2017 IDScan.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  Support: support@idscan.net
 */
#ifndef PDF417_PDF417LIB_H
#define PDF417_PDF417LIB_H

#include <cstdint>
#include <cstdlib>

#define PDF417_HANDLE uintptr_t

// Flags.
#define PDF417_FLAGS_EMPTY 0x00
#define PDF417_FLAG_BARCODE_DETECTED 0x02
#define PDF417_FLAG_BARCODE_DECODED 0x04
#define PDF417_FLAG_ERROR    0x08

#if defined(_WIN32) || defined(__WIN32__)
#  if defined(PDF417_DLPEXPORTS)
#    define  PDF417API __declspec(dllexport)
#  else
#    define  PDF417API __declspec(dllimport)
#  endif // MyLibrary_EXPORTS
#else
#  define PDF417API
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Create instance of PDF417 barcode reader.
 *
 * @return handle of the PDF417 reader.
 */
extern PDF417API PDF417_HANDLE pdf417_Create();

/**
 * Release the instance of PDF417 reader.
 *
 * @param handle handle of the PDF417 reader.
 */
extern PDF417API void pdf417_Release(PDF417_HANDLE handle);

/**
 * Read Barcode data from the grayscale image.
 *
 * @param reader handle of the reader.
 * @param img the image data in grayscale format.
 * @param w width of the image.
 * @param h height of the image.
 * @param out output byte array.
 * @param out_len size of the output array. After execution it will contains number of bytes in barcode data or 0
 * if an error has occurred.
 * @return combination of (PDF417_FLAG_BARCODE_DETECTED, PDF417_FLAG_BARCODE_DECODED) if a barcode has been found
 * and decoded, PDF417_FLAGS_EMPTY if barcode hasn't been found or PDF417_FLAG_ERROR if an error has occurred.
 */
extern PDF417API int
pdf417_Read(PDF417_HANDLE reader, uint8_t *img, uint32_t w, uint32_t h, uint8_t *out, size_t *out_len);

/**
 * Read Barcode data from the RGB image.
 *
 * @param reader handle of the reader.
 * @param img the image data in RGB format.
 * @param w width of the image.
 * @param h height of the image.
 * @param stride size of the image row in bytes.
 * @param out output byte array.
 * @param out_len size of the output array. After execution it will contains number of bytes in barcode data or 0 if an
 * error has occurred.
 * @return combination of (PDF417_FLAG_BARCODE_DETECTED, PDF417_FLAG_BARCODE_DECODED) if a barcode has been found
 * and decoded, PDF417_FLAGS_EMPTY if barcode hasn't been found or PDF417_FLAG_ERROR if an error has occurred.
 */
extern PDF417API int
pdf417_ReadRGB(PDF417_HANDLE reader, uint8_t *img, uint32_t w, uint32_t h, size_t stride, uint8_t *out,
               size_t *out_len);

#ifdef __cplusplus
}
#endif

#endif //PDF417_PDF417LIB_H
