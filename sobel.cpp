/*
Compile with emcc with:
em++ -o sobel.js sobel.cpp  -lm -O3 -s WASM=1 -s EXPORT_NAME='SOBEL' -s MODULARIZE=1 -s EXPORTED_FUNCTIONS="['_rgba2Gray', '_evaluateRgbaBorder', '_evaluateGrayBorder',  '_evaluateBlur', '_malloc']" -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall']"
*/
#include <cmath>
#include <cinttypes>
#include <memory>
#include <algorithm>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

void rgba2Gray(const uint8_t * from, uint8_t* to, size_t pixels, int tresh);
void evaluateRgbaBorder(const uint8_t *data, size_t width, size_t height, size_t border, uint8_t* out);
void evaluateGrayBorder(const uint8_t *data, size_t width, size_t height, size_t border, uint8_t* out);
float evaluateBlur(uint8_t *data, size_t width, size_t height);

#ifdef __cplusplus
}
#endif

//----------------------------------------------------------------------------------------------------------------------
void rgba2Gray(const uint8_t * from, uint8_t* to, size_t pixels, int tresh) {
    for(size_t i = 0; i < pixels; i++, from += 4, to++) {
        int summ = from[0] + from[1] + from[2];
        summ = summ / 3;

        *to = (summ >= tresh) ? (uint8_t)summ : (uint8_t)0;
    }
}

//----------------------------------------------------------------------------------------------------------------------
inline float distRGBA(const uint8_t *p1, const uint8_t *p2) {
    int r = ((int)p1[0] - (int)p2[0]);
    int g = ((int)p1[1] - (int)p2[1]);
    int b = ((int)p1[2] - (int)p2[2]);
    return sqrt(r * r + g * g + b * b);
}

//----------------------------------------------------------------------------------------------------------------------
inline void binarizeGray(uint8_t *src, size_t size, uint8_t tresh) {
    for(size_t i = 0; i < size; i++) {
        src[i] = (src[i] > tresh) ? 0xFF : 0x00;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void sobelGray(const uint8_t *src, uint8_t *dst, size_t width, size_t height) {
    if(height == 0 || width == 0) {
        return;
    }

    // Clear top and bottom line.
    memset(&dst[0], 0, width);
    memset(&dst[(height - 1) * width], 0, width);

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[width], &src[width + width]};
    for(size_t i = 1; (i + 1) < height; i++) {
        dst[i * width] = 0;
        dst[i * width + width - 1] = 0;
        for (size_t j = 1; (j + 1) < width; j++) {
            float px = (((int) lines[0][j + 1] - (int) lines[0][j - 1]) +
                     ((int) lines[1][j + 1] - (int) lines[1][j - 1]) * 2 +
                     ((int) lines[2][j + 1] - (int) lines[2][j - 1])) / (255.0f * 4);
            float py = (((int) lines[2][j - 1] - (int) lines[0][j - 1]) +
                     ((int) lines[2][j + 0] - (int) lines[0][j + 0]) * 2 +
                     ((int) lines[2][j + 1] - (int) lines[0][j + 1])) / (255.0f * 4);

            float val = (float)sqrt(px * px + py * py) / sqrt(2.0f);
            dst[i * width + j] = (uint8_t)(val * 255.0f);
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += width;
    }
}

//----------------------------------------------------------------------------------------------------------------------
void sobelRGBA(const uint8_t *src, uint8_t *dst, size_t width, size_t height) {
    const float SQRT_3 = 1.7320508075688f;

    if(height == 0 || width == 0) {
        return;
    }

    // Clear top and bottom line.
    memset(&dst[0], 0, width);
    memset(&dst[(height - 1) * width], 0, width);

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[width * 4], &src[(width + width) * 4]};
    for(size_t i = 1; (i + 1) < height; i++) {
        dst[i * width] = 0;
        dst[i * width + width - 1] = 0;
        for (size_t j = 1; (j + 1) < width; j++) {

            float px = (distRGBA(&lines[0][(j + 1) * 4], &lines[0][(j - 1) * 4]) +
                        distRGBA(&lines[1][(j + 1) * 4], &lines[1][(j - 1) * 4]) * 2 +
                        distRGBA(&lines[2][(j + 1) * 4], &lines[2][(j - 1) * 4])) / (SQRT_3 * 255.0f * 4.0f);

            float py = (distRGBA(&lines[2][(j - 1) * 4], &lines[0][(j - 1) * 4]) +
                        distRGBA(&lines[2][(j + 0) * 4], &lines[0][(j + 0) * 4]) * 2 +
                        distRGBA(&lines[2][(j + 1) * 4], &lines[0][(j + 1) * 4])) / (SQRT_3 * 255.0f * 4.0f);

            float val = (float)sqrt(px * px + py * py) / sqrt(2.0f);
            dst[i * width + j] = (uint8_t)(val * 255.0f);
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += width * 4;
    }
}

//----------------------------------------------------------------------------------------------------------------------
float avgGray(const uint8_t *src, const uint8_t *mask, size_t size) {
    uint64_t summ = 0;
    size_t total = 0;
    for(size_t i = 0; i < size; i++) {
        if(mask[i] > 0x7F) {
            summ += src[i];
            total++;
        }
    }

    return (total > 0) ? (float)summ / (float)total : 0.0f;
}

//----------------------------------------------------------------------------------------------------------------------
float stdGray(const uint8_t *src, size_t size) {
    if(size == 0) {
        return 0.0f;
    }

    uint64_t summ = 0;
    for(size_t i = 0; i < size; i++) {
        summ += src[i];
    }

    float avg = (float)summ / (float)size;
    float summ2 = 0.0f;
    for(size_t i = 0; i < size; i++) {
        float d = avg - (float)src[i];
        summ2 += d * d;
    }

    return sqrt(summ / size);
}

//----------------------------------------------------------------------------------------------------------------------
void blurX(uint8_t *data, size_t width, size_t height) {
    float kernel[5] = {0.06136, 0.24477, 0.38774, 0.24477, 0.06136};
    uint8_t values[5] = {data[width - 3], data[width - 2], data[0], data[1], data[2]};
    size_t total = width * height - 3;

    for(size_t i = 0; i < total; i++) {
        data[i] = (uint8_t)(values[0] * kernel[0] +
                            values[1] * kernel[1] +
                            values[2] * kernel[2] +
                            values[3] * kernel[3] +
                            values[4] * kernel[4]);

        values[0] = values[1];
        values[1] = values[2];
        values[2] = values[3];
        values[3] = values[4];
        values[4] = data[i + 3];
    }
}

//----------------------------------------------------------------------------------------------------------------------
void blurY(uint8_t *data, size_t width, size_t height) {
    float kernel[5] = {0.06136, 0.24477, 0.38774, 0.24477, 0.06136};

    for(size_t i = 0; i < width; i++, data++) {
        uint8_t values[5] = {data[0], data[0], data[0], data[width], data[width + width]};
        for(size_t j = 0; j < height; j++) {
            data[j * width] = (uint8_t)
                    (
                            values[0] * kernel[0] +
                            values[1] * kernel[1] +
                            values[2] * kernel[2] +
                            values[3] * kernel[3] +
                            values[4] * kernel[4]
                    );

            values[0] = values[1];
            values[1] = values[2];
            values[2] = values[3];
            values[3] = values[4];
            values[4] = data[min((j + 3), height - 1) * width];
        }
    }
}

//----------------------------------------------------------------------------------------------------------------------
float evaluateGrayBorderY(const uint8_t *src, size_t width, size_t height, size_t stride) {
    const float SOBEL_TRESH = 0.10f;
    if(height == 0 || width == 0) {
        return 0.0f;
    }

    uint64_t  summ = 0;

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[stride], &src[stride + stride]};
    for(size_t i = 1; (i + 1) < height; i++) {
        for (size_t j = 1; (j + 1) < width; j++) {
            float px = (((int) lines[0][j + 1] - (int) lines[0][j - 1]) +
                        ((int) lines[1][j + 1] - (int) lines[1][j - 1]) * 2 +
                        ((int) lines[2][j + 1] - (int) lines[2][j - 1])) / (255.0f * 4);

            if(px > SOBEL_TRESH) {
                summ++;
            }
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += stride;
    }

    return (float)summ / (float)(width * height);
}

//----------------------------------------------------------------------------------------------------------------------
float evaluateGrayBorderX(const uint8_t *src, size_t width, size_t height, size_t stride) {
    const float SOBEL_TRESH = 0.10f;
    if(height == 0 || width == 0) {
        return 0.0f;
    }

    uint64_t  summ = 0;

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[stride], &src[stride + stride]};
    for(size_t i = 1; (i + 1) < height; i++) {
        for (size_t j = 1; (j + 1) < width; j++) {
            float py = (((int) lines[2][j - 1] - (int) lines[0][j - 1]) +
                        ((int) lines[2][j + 0] - (int) lines[0][j + 0]) * 2 +
                        ((int) lines[2][j + 1] - (int) lines[0][j + 1])) / (255.0f * 4);

            if(py > SOBEL_TRESH) {
                summ++;
            }
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += stride;
    }

    return (float)summ / (float)(width * height);
}

//----------------------------------------------------------------------------------------------------------------------
float evaluateRgbaBorderY(const uint8_t *src, size_t width, size_t height, size_t stride) {
    const float SQRT_3 = 1.7320508075688f;
    const float SOBEL_TRESH = 0.10f;

    if(height == 0 || width == 0) {
        return 0.0f;
    }

    uint64_t  summ = 0;

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[stride], &src[stride + stride]};
    for(size_t i = 1; (i + 1) < height; i++) {
        for (size_t j = 1; (j + 1) < width; j++) {
            float px = (distRGBA(&lines[0][(j + 1) * 4], &lines[0][(j - 1) * 4]) +
                        distRGBA(&lines[1][(j + 1) * 4], &lines[1][(j - 1) * 4]) * 2 +
                        distRGBA(&lines[2][(j + 1) * 4], &lines[2][(j - 1) * 4])) / (SQRT_3 * 255.0f * 4.0f);

            if(px > SOBEL_TRESH) {
                summ++;
            }
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += stride;
    }

    return (float)summ / (float)(width * height);
}

//----------------------------------------------------------------------------------------------------------------------
float evaluateRgbaBorderX(const uint8_t *src, size_t width, size_t height, size_t stride) {
    const float SOBEL_TRESH = 0.10f;
    const float SQRT_3 = 1.7320508075688f;

    if(height == 0 || width == 0) {
        return 0.0f;
    }

    uint64_t  summ = 0;

    // Calculate Sobel.
    const uint8_t* lines[3] = {&src[0], &src[stride], &src[stride + stride]};
    for(size_t i = 1; (i + 1) < height; i++) {
        for (size_t j = 1; (j + 1) < width; j++) {
            float py = (distRGBA(&lines[2][(j - 1) * 4], &lines[0][(j - 1) * 4]) +
                        distRGBA(&lines[2][(j + 0) * 4], &lines[0][(j + 0) * 4]) * 2 +
                        distRGBA(&lines[2][(j + 1) * 4], &lines[0][(j + 1) * 4])) / (SQRT_3 * 255.0f * 4.0f);

            if(py > SOBEL_TRESH) {
                summ++;
            }
        }

        lines[0] = lines[1];
        lines[1] = lines[2];
        lines[2] += stride;
    }

    return (float)summ / (float)(width * height);
}

//----------------------------------------------------------------------------------------------------------------------
void evaluateGrayBorder(const uint8_t *data, size_t width, size_t height, size_t border, uint8_t* out) {
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;

    if(width == 0 || height == 0 || border == 0) {
        return;
    }
    border = min(min(border, width - 1), height - 1);

    const uint8_t* gray = &data[0];
    // Top
    out[2] = (uint8_t)(255.0f * evaluateGrayBorderX(&gray[0], width, border, width));
    // Bottom
    out[3] = (uint8_t)(255.0f * evaluateGrayBorderX(&gray[(height - border) * width], width, border, width));
    // Left
    out[0] = (uint8_t)(255.0f * evaluateGrayBorderY(&gray[0], border, height, width));
    // Right
    out[1] = (uint8_t)(255.0f * evaluateGrayBorderY(&gray[width - border], border, height, width));
}

//----------------------------------------------------------------------------------------------------------------------
void evaluateRgbaBorder(const uint8_t *data, size_t width, size_t height, size_t border, uint8_t* out) {
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;

    if(width == 0 || height == 0 || border == 0) {
        return;
    }
    border = min(min(border, width - 1), height - 1);

    const uint8_t* rgba = &data[0];
    // Top
    out[2] = (uint8_t)(255.0f * evaluateRgbaBorderX(&rgba[0], width, border, width * 4));
    // Bottom
    out[3] = (uint8_t)(255.0f * evaluateRgbaBorderX(&rgba[(height - border) * width * 4], width, border, width * 4));
    // Left
    out[0] = (uint8_t)(255.0f * evaluateRgbaBorderY(&rgba[0], border, height, width * 4));
    // Right
    out[1] = (uint8_t)(255.0f * evaluateRgbaBorderY(&rgba[(width - border) * 4], border, height, width * 4));
}

//----------------------------------------------------------------------------------------------------------------------
float evaluateBlur(uint8_t *data, size_t width, size_t height) {
    const float MAGIC_REF = 41.6998f;

    uint8_t* gray = &data[0];
    uint8_t* sobel1 = &data[1 * width * height];
    uint8_t* mask = &data[2 * width * height];
    uint8_t* sobel2 = &data[3 * width * height];

    // Check std.
    float s = stdGray(gray, width * height);
    if(s < 2.0f) {
        return 0.0f;
    }

    // Sobel 2
    sobelGray(gray, sobel1, width, height);

    // Mask
    memcpy(mask, sobel1, width * height);
    binarizeGray(mask, width * height, 10);

    // Blur.
    blurX(gray, width, height);
    blurY(gray, width, height);

    // Sobel 2
    sobelGray(gray, sobel2, width, height);

    // Evaluate blur.
    float sobelAvg1 = avgGray(sobel1, mask, width * height);
    float sobelAvg2 = avgGray(sobel2, mask, width * height);

    return max((sobelAvg1 - sobelAvg2) / MAGIC_REF, 0.0f);
}
