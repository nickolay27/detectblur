/*
Compile with emcc with:
emcc -o sobel.js sobel.c  -lm -O3 -s WASM=1 -s EXPORTED_FUNCTIONS="['_sobel', '_detectBlur', '_detectBlur2', '_malloc']" -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall']"
*/
#include<stdio.h>
#include<math.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>

//----------------------------------------------------------------------------------------------------------------------
inline unsigned char getPixel(unsigned char *data, int i, int j, int width) {
    int summ = data[(i * width + j) * 4 + 0];
    summ += data[(i * width + j) * 4 + 1];
    summ += data[(i * width + j) * 4 + 2];
  return (unsigned char)(summ / 3);
}

//----------------------------------------------------------------------------------------------------------------------
void sobel(unsigned char *dst, unsigned char *src, int width, int height) {
  int pixelX, pixelY, mag;

  for (int i = 1; i < height - 1; i++) {
    for (int j = 1; j < width - 1; j++) {
      pixelX = (-1 * getPixel(src, i - 1, j - 1, width)) +
        (1 * getPixel(src, i - 1, j + 1, width)) +
        (-2 * getPixel(src, i, j - 1, width)) +
        (2 * getPixel(src, i, j + 1, width)) +
        (-1 * getPixel(src, i + 1, j - 1, width)) +
        (1 * getPixel(src, i + 1, j + 1, width));

      pixelY = (-1 * getPixel(src, i - 1, j - 1, width)) +
        (-2 * getPixel(src, i - 1, j, width)) +
        (-1 * getPixel(src, i - 1, j + 1, width)) +
        (1 * getPixel(src, i + 1, j - 1, width)) +
        (2 * getPixel(src, i + 1, j, width)) +
        (1 * getPixel(src, i + 1, j + 1, width));

      mag = ceil(sqrt(pixelX * pixelX + pixelY * pixelY));

      if (mag > 255) {
        mag = 255;
      }

      dst[i * width + j] = mag;
    }
  }
}

//----------------------------------------------------------------------------------------------------------------------
float detectBlur(const unsigned char* pixels, int width, int height) {
    int numEdges = 0;
    int sumEdgeWidths = 0;
    int edgeIntensThresh = 60;

    for (int y = 0; y < height; y++) {
        // Reset edge marker, none found yet
        int edgeStart = -1;
        unsigned char oldValue = 0;
        for (int x = 1; x < width; x++) {
            unsigned char value = pixels[y * width + x];
            // Edge is still open
            if (edgeStart >= 0 && x > edgeStart) {
                oldValue = pixels[y * width + x - 1];
                // Value stopped increasing => edge ended
                if (value < oldValue) {
                    // Only count edges that reach a certain intensity
                    if (oldValue >= edgeIntensThresh) {
                        int edgeWidth = x - edgeStart - 1;
                        numEdges += 1;
                        sumEdgeWidths += edgeWidth;
                    }
                    edgeStart = -1; // Reset edge marker
                }
            }
            // Edge starts
            if (value == 0) {
                edgeStart = x;
            }
        }
    }

    if (numEdges == 0) {
        return 0.0f;
    } else {
        return ((float)sumEdgeWidths / (float)numEdges) / (float)width * 100.0f;
    }
}


//----------------------------------------------------------------------------------------------------------------------
float detectBlur2(const unsigned char* pixels, int width, int height) {
    int numEdges = 0;
    float  sumEdgeWidths = 0;
    int edgeIntensThresh = 2;

    for (int y = 0; y < height; y++) {
        // Reset edge marker, none found yet
        int state = 0;
        float edgeSize = 0;
        float edgeStrange = 0;
        for (int x = 1; x < width; x++) {
            unsigned char value = pixels[y * width + x];
            switch (state) {
                case 0:
                    if(value > edgeIntensThresh) {
                        edgeSize = 1;
                        edgeStrange = (float)value / 255.0f;
                        state = 1;
                    }
                    break;
                case 1:
                    if(value > edgeIntensThresh) {
                        edgeSize++;
                        edgeStrange += (float)value / 255.0f;
                    } else {
                        state = 0;
                        numEdges += 1;
                        sumEdgeWidths += edgeStrange;
                    }
                    break;
            }
        }
    }

    if (numEdges == 0) {
        return 0.0f;
    } else {
        return ((float)sumEdgeWidths / (float)numEdges) / (float)width * 100.0f;
    }
}
