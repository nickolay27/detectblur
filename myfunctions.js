function detectBlur (pixels){
    var x; var y; var value; var oldValue; var edgeStart; var edgeWidth; var bm; var percWidth
    var width = pixels[0].length
    var height = pixels.length
    var numEdges = 0
    var sumEdgeWidths = 0
    var edgeIntensThresh = 60

    for (y = 0; y < height; y += 1) {
        // Reset edge marker, none found yet
        edgeStart = -1
        for (x = 0; x < width; x += 1) {
            value = pixels[y][x]
            // Edge is still open
            if (edgeStart >= 0 && x > edgeStart) {
                oldValue = pixels[y][x - 1]
                // Value stopped increasing => edge ended
                if (value < oldValue) {
                    // Only count edges that reach a certain intensity
                    if (oldValue >= edgeIntensThresh) {
                        edgeWidth = x - edgeStart - 1
                        numEdges += 1
                        sumEdgeWidths += edgeWidth
                    }
                    edgeStart = -1 // Reset edge marker
                }
            }
            // Edge starts
            if (value === 0) {
                edgeStart = x
            }
        }
    }

    if (numEdges === 0) {
        bm = 0
        percWidth = 0
    } else {
        bm = sumEdgeWidths / numEdges
        percWidth = bm / width * 100
    }
    return {
        width: width,
        height: height,
        num_edges: numEdges,
        avg_edge_width: bm,
        avg_edge_width_perc: percWidth
    }
}
function reducedPixels (imageData){
    var i; var x; var y; var row
    var pixels = imageData.data
    var rowLen = imageData.width * 4
    var rows = []

    for (y = 0; y < pixels.length; y += rowLen) {
        row = new Uint8ClampedArray(imageData.width)
        x = 0
        for (i = y; i < y + rowLen; i += 4) {
            row[x] = pixels[i]
            x += 1
        }
        rows.push(row)
    }
    return rows
}
